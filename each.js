module.exports = function (arr,callback){
    for(let i = 0; i < arr.length; i += 1){
        let val = arr[i];
        callback( val, i, arr);
    }
}
