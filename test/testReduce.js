const reduce = require('../reduce');

const arr = [1,2,3,4,5,5]; 
let res = reduce(arr, function(start, val){
    return val ? start + val : start;
},5);
console.log(res);