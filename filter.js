module.exports = function(arr, callback){
    const res = []
    for(i = 0; i< arr.length; i++){
        if(callback(arr[i], i, arr)){
            res.push(arr[i]);
        }
    }
    return res
}
