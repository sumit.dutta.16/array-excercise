module.exports = function(arr, callback){
    let res;
    for(let i = 0; i < arr.length; i++){
        if(callback(arr[i], i, arr)){
            return arr[i];
        }
    }
    return res;
}
