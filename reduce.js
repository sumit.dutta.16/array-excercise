module.exports = function(arr, callback, startVal){
    let i = 0;
    if(startVal === undefined){
        startVal = arr[0];
        i = 1;
    }
    for(i; i < arr.length; i++){
        startVal = callback(startVal, arr[i], i, arr);
    }
    return startVal;
}